// Pomiar Wiązki w Linii Iniekcyjnej - TI TM4C123GH6PM
// Copyright (C) 2019 Ksawery Wieczorkowski-Rettinger <kwrettinger@gmail.com>

/* ----------------------- Includes -----------------------------------------------*/

#include <stdio.h>
#include "tm4c123gh6pm.h"
#include "mb.h"
#include "mbreg.h"
#include "mbutils.h"
#include "port.h"
#include "params.h"

/* ----------------------- Function prototypes ------------------------------------*/

void   xI2CReceiveADCReading(USHORT * usADCReadingBufCur, UCHAR ucReadingNum, ULONG * ulErrorI2C);
void   xI2CMasterWait(ULONG * pulErrorI2C);
void   xUpdateStatusRegADCError(UCHAR ucChannel, BOOL bError);
USHORT usADCGetAverage(USHORT * usArray, UCHAR ucLength);

/* ----------------------- Static variables ------------------------------------*/

static ULONG  ulTimerI2CLoadValue = 0;
static UCHAR  ucADCStatus = 0;
static BOOL   bI2CTimeout = FALSE;

int main(void) {

    /*------------------------ Variable declarations ------------------------------------*/

    USHORT usADCReadingBuf[ADC_NUM_READINGS] = {0};
    USHORT usADCReadingAverage = 0;
    ULONG  ulErrorI2C = I2C_MASTER_ERR_NONE;
    UCHAR  ucMotorStatus = 0;
    UCHAR  ucCoilStatus = 0;
    UCHAR  ucChannel = 0;
    UCHAR  ucReadingNum = 0;
    UCHAR  ucSCLCycle = 0;
    BOOL   bInitADC = TRUE;
    BOOL   bFirstReading = TRUE;
    BOOL   bSwitchChannel = FALSE;
    BOOL   bHighSensitivity = FALSE;

    /*------------------------ System Clock Configuration (50MHz) -----------------------*/

    SysCtlClockSet(SYSCTL_SYSDIV_4 | SYSCTL_USE_PLL | SYSCTL_OSC_MAIN | SYSCTL_XTAL_16MHZ);

    /*------------------------ GPIO Pin Configuration -----------------------------------*/

    //Enable GPIO peripherals
    SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOA);
    SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOB);
    SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOC);
    SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOE);

    // UART
    GPIOPinConfigure(GPIO_PA0_U0RX);
    GPIOPinConfigure(GPIO_PA1_U0TX);
    GPIOPinTypeUART(GPIO_PORTA_BASE, GPIO_PIN_0 | GPIO_PIN_1);

    // External LED
    GPIOPinTypeGPIOOutput(GPIO_PORTB_BASE, GPIO_PIN_6);

    // I2C
    GPIOPinConfigure(GPIO_PB2_I2C0SCL);
    GPIOPinConfigure(GPIO_PB3_I2C0SDA);
    GPIOPinTypeI2CSCL(GPIO_PORTB_BASE, GPIO_PIN_2);
    GPIOPinTypeI2C(GPIO_PORTB_BASE, GPIO_PIN_3);

    // Configuration pin
    GPIOPinTypeGPIOOutput(GPIO_PORTA_BASE, GPIO_PIN_4);

    // ADC sensitivity switching
    GPIOPinTypeGPIOOutput(GPIO_PORTB_BASE, GPIO_PIN_7);
    GPIOPinWrite(GPIO_PORTB_BASE, GPIO_PIN_7, GPIO_PIN_7); //default sensitivity

    // ADC channel switching
    GPIOPinTypeGPIOOutput(GPIO_PORTB_BASE, GPIO_PIN_4);
    GPIOPinTypeGPIOOutput(GPIO_PORTE_BASE, GPIO_PIN_1);
    GPIOPinWrite(GPIO_PORTB_BASE, GPIO_PIN_4, 0);
    GPIOPinWrite(GPIO_PORTE_BASE, GPIO_PIN_1, 0);

    //Motor status input
    GPIOPinTypeGPIOInput(GPIO_PORTA_BASE, GPIO_PIN_6);

    //Safety lock input
    GPIOPinTypeGPIOInput(GPIO_PORTA_BASE, GPIO_PIN_7);

    /*------------------------------ I2C Timeout Timer ----------------------------------*/

    SysCtlPeripheralEnable(SYSCTL_PERIPH_TIMER1);
    TimerConfigure(TIMER1_BASE, TIMER_CFG_ONE_SHOT);
    IntEnable(INT_TIMER1A);
    TimerIntEnable(TIMER1_BASE, TIMER_TIMA_TIMEOUT);
    ulTimerI2CLoadValue = (SysCtlClockGet() / I2C_SPEED) * I2C_MASTER_TIMEOUT;

    /*------------------------------ I2C Configuration ----------------------------------*/

    //Initialise Master module
    SysCtlPeripheralEnable(SYSCTL_PERIPH_I2C0);
    I2CMasterInitExpClk(I2C0_BASE, SysCtlClockGet(), FALSE); //standard speed

    /*------------------------------ MODBUS Initialization ------------------------------*/

    //Initialize MODBUS protocol stack with parameters defined in params.h
    (void) eMBInit(MB_MODE, MB_SLAVEID, MB_PORT, MB_BAUDRATE, MB_PARITY);

     //Enable the MODBUS Protocol Stack.
    (void) eMBEnable();

    //Read motor status pin and update coils on initialization
    ucMotorStatus = (UCHAR)((GPIOPinRead(GPIO_PORTA_BASE, GPIO_PIN_6) >> 6) & 1);
    (void) eMBRegCoilsCB(&ucMotorStatus, (REG_COILS_START + 1), 1, MB_REG_WRITE);
    (void) eMBRegCoilsCB(&ucMotorStatus, (REG_COILS_START + 3), 1, MB_REG_WRITE);

    while (1)
    {
        /*------------------------------ MODBUS Polling -------------------------------------*/

        (void) eMBPoll();

        /*------------------------------ I2C Protocol ---------------------------------------*/

        if (!I2CMasterBusBusy(I2C0_BASE))
        {
            ulErrorI2C = I2C_MASTER_ERR_NONE;

            /*-------------------------- ADC Initialization -------------------------------------*/

            if (bInitADC)
            {
                //Send mode
                I2CMasterSlaveAddrSet(I2C0_BASE, I2C_ADDRESS, FALSE);

                //Send configuration byte
                I2CMasterDataPut(I2C0_BASE, I2C_CONFIG);
                I2CMasterControl(I2C0_BASE, I2C_MASTER_CMD_SINGLE_SEND);
                xI2CMasterWait(&ulErrorI2C);
                if (ulErrorI2C == I2C_MASTER_ERR_NONE) bInitADC = FALSE;

                //Recieve mode
                I2CMasterSlaveAddrSet(I2C0_BASE, I2C_ADDRESS, TRUE);
            }

            /*-------------------------- ADC channel select -------------------------------------*/

            if (bSwitchChannel)
            {
                ucChannel++;
                if (ucChannel == ADC_NUM_CHANNELS) ucChannel = 0;
                GPIOPinWrite(GPIO_PORTB_BASE, GPIO_PIN_7, GPIO_PIN_7); //default sensitivity

                switch (ucChannel)
                {
                case 0: //channel 1
                    GPIOPinWrite(GPIO_PORTB_BASE, GPIO_PIN_4, 0);
                    GPIOPinWrite(GPIO_PORTE_BASE, GPIO_PIN_1, 0);
                    break;
                case 1: //channel 2
                    GPIOPinWrite(GPIO_PORTB_BASE, GPIO_PIN_4, 0);
                    GPIOPinWrite(GPIO_PORTE_BASE, GPIO_PIN_1, GPIO_PIN_1);
                    break;
                case 2: //channel 3
                    GPIOPinWrite(GPIO_PORTB_BASE, GPIO_PIN_4, GPIO_PIN_4);
                    GPIOPinWrite(GPIO_PORTE_BASE, GPIO_PIN_1, 0);
                    break;
                case 3: //channel 4
                    GPIOPinWrite(GPIO_PORTB_BASE, GPIO_PIN_4, GPIO_PIN_4);
                    GPIOPinWrite(GPIO_PORTE_BASE, GPIO_PIN_1, GPIO_PIN_1);
                    break;
                }

                ucReadingNum = 0;
                bFirstReading = TRUE;
                bSwitchChannel = FALSE;
                bHighSensitivity = FALSE;
            }

            /*-------------------------- Read from ADC ------------------------------------------*/

            xI2CReceiveADCReading(usADCReadingBuf, ucReadingNum, &ulErrorI2C);

            if (ulErrorI2C != I2C_MASTER_ERR_NONE) //I2C error
            {
                xUpdateStatusRegADCError(ucChannel, TRUE); //update Modbus error register
                bSwitchChannel = TRUE; //switch ADC channel
                continue;
            }

            if (!(ucADCStatus >> ADC_STATUS_BIT) & 0x01) //check if latest reading is new
            {
                if (ucReadingNum == 0) //first reading
                {
                    if (bFirstReading && ((usADCReadingBuf[ucReadingNum] | ADC_PRECISION_MASK) == ADC_PRECISION_MASK))
                    {
                        GPIOPinWrite(GPIO_PORTB_BASE, GPIO_PIN_7, 0); //increase sensitivity
                        bHighSensitivity = TRUE;
                        bFirstReading = FALSE;
                    }
                    else ucReadingNum++;
                }
                else
                {
                    ucReadingNum++;

                    if (ucReadingNum == ADC_NUM_READINGS) //all readings collected
                    {
                        //Calculate average
                        usADCReadingAverage = usADCGetAverage(usADCReadingBuf, ADC_NUM_READINGS);

                        //Increased ADC sensitivity, divide result by 10
                        if (bHighSensitivity) usADCReadingAverage = ((usADCReadingAverage + 5) / 10);

                        //Store results in Modbus register
                        xInternalRegInputReadWrite(&usADCReadingAverage, REG_INPUT_START + REG_ADDR_OFFSET + ucChannel, 1, TRUE);
                        xUpdateStatusRegADCError(ucChannel, FALSE);

                        //Switch ADC channel
                        bSwitchChannel = TRUE;
                    }
                }
            }
        }
        else //force ADC to release the SDA line
        {
            GPIOPinTypeGPIOOutput(GPIO_PORTB_BASE, GPIO_PIN_2);
            GPIOPinTypeGPIOOutput(GPIO_PORTB_BASE, GPIO_PIN_3);

            //Simulate SCL clock cycles
            for (ucSCLCycle = 0; ucSCLCycle < I2C_RESET_CYCLES; ucSCLCycle++)
            {
                GPIOPinWrite(GPIO_PORTB_BASE, GPIO_PIN_2, 0); //SCL line low
                SysCtlDelay(100);
                GPIOPinWrite(GPIO_PORTB_BASE, GPIO_PIN_2, GPIO_PIN_2); //SCL line high
                SysCtlDelay(100);
            }

            //Simulate STOP condition
            GPIOPinWrite(GPIO_PORTB_BASE, GPIO_PIN_2, 0); //SCL line low
            GPIOPinWrite(GPIO_PORTB_BASE, GPIO_PIN_3, 0); //SDA line low
            SysCtlDelay(100);
            GPIOPinWrite(GPIO_PORTB_BASE, GPIO_PIN_2, GPIO_PIN_2); //SCL line high
            GPIOPinWrite(GPIO_PORTB_BASE, GPIO_PIN_3, GPIO_PIN_3); //SDA line high

            //Reconfigure pins to I2C
            GPIOPinConfigure(GPIO_PB2_I2C0SCL);
            GPIOPinConfigure(GPIO_PB3_I2C0SDA);
            GPIOPinTypeI2CSCL(GPIO_PORTB_BASE, GPIO_PIN_2);
            GPIOPinTypeI2C(GPIO_PORTB_BASE, GPIO_PIN_3);
        }

        /*-------------------------- Safety Lock ----------------------------------------*/

        //Read safety lock status
        if (GPIOPinRead(GPIO_PORTA_BASE, GPIO_PIN_7) & (1 << 7))
        {
            //Update status and safety lock coils
            //NOTE: this overrides any configuration commands recieved via Modbus.
            ucCoilStatus = 0x03;
            (void) eMBRegCoilsCB(&ucCoilStatus, (REG_COILS_START + 1), 2, MB_REG_WRITE);
        }
        else
        {
            //Update safety lock coil
            ucCoilStatus = 0x00;
            (void) eMBRegCoilsCB(&ucCoilStatus, (REG_COILS_START + 2), 1, MB_REG_WRITE);
        }

        /*-------------------------- Configuration Command ------------------------------*/

        //Read configuration coil
        (void) eMBRegCoilsCB(&ucCoilStatus, (REG_COILS_START + 1), 1, MB_REG_READ);

        //Write configuration coil status to pin
        if (xMBUtilGetBits(&ucCoilStatus, 0, 1) == 0x01)
        {
            GPIOPinWrite(GPIO_PORTA_BASE, GPIO_PIN_4, GPIO_PIN_4);
        }
        else
        {
            GPIOPinWrite(GPIO_PORTA_BASE, GPIO_PIN_4, 0);
        }

        /*-------------------------- Motor status ---------------------------------------*/

        //Read motor status pin and update coil status
        ucMotorStatus = (UCHAR)((GPIOPinRead(GPIO_PORTA_BASE, GPIO_PIN_6) >> 6) & 1);
        (void) eMBRegCoilsCB(&ucMotorStatus, (REG_COILS_START + 3), 1, MB_REG_WRITE);
    }
}

void xI2CReceiveADCReading(USHORT * usADCReadingBufCur, UCHAR ucReadingNum, ULONG * pulErrorI2C)
{
    /*----------------- Initiate I2C transaction (First Data Byte) -------------------*/

    I2CMasterControl(I2C0_BASE, I2C_MASTER_CMD_BURST_RECEIVE_START);
    xI2CMasterWait(pulErrorI2C); //wait for response

    if (*pulErrorI2C != I2C_MASTER_ERR_NONE)
    {
        //Error or timeout occured, stop transaction
        I2CMasterControl(I2C0_BASE, I2C_MASTER_CMD_BURST_RECEIVE_ERROR_STOP);
        return;
    }
    else
    {
        //Read from FIFO
        usADCReadingBufCur[ucReadingNum] = (USHORT)(I2CMasterDataGet(I2C0_BASE) << 8);
    }

    /*---------------- Continue I2C transaction (Second Data Byte) -------------------*/

    I2CMasterControl(I2C0_BASE, I2C_MASTER_CMD_BURST_RECEIVE_CONT);
    xI2CMasterWait(pulErrorI2C); //wait for response

    if (*pulErrorI2C != I2C_MASTER_ERR_NONE)
    {
        //Error or timeout occured, stop transaction
        I2CMasterControl(I2C0_BASE, I2C_MASTER_CMD_BURST_RECEIVE_ERROR_STOP);
        return;
    }
    else
    {
        //Read from FIFO
        usADCReadingBufCur[ucReadingNum] |= (USHORT)I2CMasterDataGet(I2C0_BASE);
    }

    /*------------------- Finalize I2C transaction (Status Byte) ---------------------*/

    I2CMasterControl(I2C0_BASE, I2C_MASTER_CMD_BURST_RECEIVE_FINISH);
    xI2CMasterWait(pulErrorI2C); //wait for response

    if (*pulErrorI2C != I2C_MASTER_ERR_NONE)
    {
        //Error or timeout occured, stop transaction
        I2CMasterControl(I2C0_BASE, I2C_MASTER_CMD_BURST_RECEIVE_ERROR_STOP);
        return;
    }
    else
    {
        //Read status byte from FIFO
        ucADCStatus = I2CMasterDataGet(I2C0_BASE);
    }

    return;
}

void xI2CMasterWait(ULONG * pulErrorI2C)
{
    //Start I2C timeout timer
    bI2CTimeout = FALSE;
    TimerLoadSet(TIMER1_BASE, TIMER_BOTH, ulTimerI2CLoadValue);
    TimerEnable(TIMER1_BASE, TIMER_BOTH);

    //Wait for response
    while (I2CMasterBusy(I2C0_BASE))
    {
        if (bI2CTimeout) //check for timeout
        {
            //Timeout occured
            *pulErrorI2C = I2C_MASTER_ERR_CLK_TOUT;
            return;
        }
    }

    TimerDisable(TIMER1_BASE, TIMER_BOTH); //response received, switch off timer

    //Check for other errors
    *pulErrorI2C = I2CMasterErr(I2C0_BASE);
    return;
}

void xUpdateStatusRegADCError(UCHAR ucChannel, BOOL bError)
{
    USHORT usErrorReg = 0;

    (void) xInternalRegInputReadWrite(&usErrorReg, REG_ERROR_ADDR + REG_ADDR_OFFSET, 1, FALSE); //read status register

    if (bError)
    {
        usErrorReg |= (1 << ucChannel);
    }
    else
    {
        usErrorReg &= ~(1 << ucChannel);
    }

    (void) xInternalRegInputReadWrite(&usErrorReg, REG_ERROR_ADDR + REG_ADDR_OFFSET, 1, TRUE); //write to status register
}

USHORT usADCGetAverage(USHORT * usArray, UCHAR ucLength)
{
    UCHAR i = 0;
    ULONG ucSum = 0;

    for (i = 0; i < ucLength; i++)
    {
        ucSum += usArray[i];
    }

    return ((ucSum + (ucLength / 2)) / ucLength); //return rounded result
}

void Timer1IntHandler(void)
{
    uint32_t ui32status;
    ui32status = TimerIntStatus(TIMER1_BASE, true);
    TimerIntClear(TIMER1_BASE, ui32status);
    bI2CTimeout = TRUE;
}

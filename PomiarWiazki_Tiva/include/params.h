/*
 * params.h
 *
 *  Created on: 16 lip 2019
 *      Author: kwret
 */

#ifndef INCLUDE_PARAMS_H_
#define INCLUDE_PARAMS_H_

/* ----------------------- Defines ------------------------------------------*/

#define MB_MODE              (MB_RTU)
#define MB_SLAVEID           (0x01)
#define MB_PORT              (0)
#define MB_BAUDRATE          (19200)
#define MB_PARITY            (MB_PAR_EVEN)

#define I2C_CONFIG           (0b00001000) //30 SPS
#define I2C_ADDRESS          (0b1001010)
#define I2C_SPEED            (100000)     //bps
#define I2C_MASTER_TIMEOUT   (20)         //I2C clock cycles
#define I2C_RESET_CYCLES     (18)

#define ADC_NUM_CHANNELS     (4)
#define ADC_NUM_READINGS     (4)
#define ADC_NUM_BYTES        (2)
#define ADC_STATUS_BIT       (7)
#define ADC_ERROR_NONE       (0x00)
#define ADC_ERROR_CH1        (0x01)
#define ADC_ERROR_CH2        (0x02)
#define ADC_ERROR_CH3        (0x04)
#define ADC_ERROR_CH4        (0x08)

#define REG_NUM_BYTES        (2)
#define REG_ADDR_OFFSET      (1)
#define REG_ERROR_ADDR       (3005)


#endif /* INCLUDE_PARAMS_H_ */

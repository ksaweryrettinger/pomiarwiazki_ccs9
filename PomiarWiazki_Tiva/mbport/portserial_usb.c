/*
 * FreeModbus Libary: TM4C123GH6PM Port
 * Copyright (C) 2019 Ksawery Wieczorkowski-Rettinger <kwrettinger@gmail.com>
 *
 * FreeModbus Libary: BARE Port
 * Copyright (C) 2006 Christian Walter <wolti@sil.at>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * File: $Id$
 */

#include <mbport/include/port.h>
#include "mb.h"
#include "mbport.h"
#include "mbdiag.h"

/* ----------------------- Start implementation -----------------------------*/
void
vMBPortSerialEnable(BOOL xRxEnable, BOOL xTxEnable)
{
    /* If xRXEnable enable serial receive interrupts. If xTxENable enable
     * transmitter empty interrupts.
     */
    if (xRxEnable)
    {
        UARTIntEnable(UART0_BASE, UART_INT_RX | UART_INT_OE);
    }
    else UARTIntDisable(UART0_BASE, UART_INT_RX | UART_INT_OE);

    if (xTxEnable)
    {
        UARTIntEnable(UART0_BASE, UART_INT_TX);
        pxMBFrameCBTransmitterEmpty();
    }
    else UARTIntDisable(UART0_BASE, UART_INT_TX);
}

BOOL
xMBPortSerialInit(UCHAR ucPORT, ULONG ulBaudRate, UCHAR ucDataBits, eMBParity eParity)
{
    (void) ucPORT;
    ULONG ulParity, ulBits, ulStopBits;
    BOOL bInitialized = TRUE;

    /* --------------------------------------------------------------------------*/

    switch (eParity)
    {
        case MB_PAR_EVEN:
            ulParity = UART_CONFIG_PAR_EVEN;
            ulStopBits = UART_CONFIG_STOP_ONE;
            break;
        case MB_PAR_ODD:
            ulParity = UART_CONFIG_PAR_ODD;
            ulStopBits = UART_CONFIG_STOP_ONE;
            break;
        case MB_PAR_NONE:
            ulParity = UART_CONFIG_PAR_NONE;
            ulStopBits = UART_CONFIG_STOP_TWO;
            break;
        default:
            bInitialized = FALSE;
    }
    switch (ucDataBits)
    {
        case 8:
            ulBits = UART_CONFIG_WLEN_8;
            break;
        case 7:
            ulBits = UART_CONFIG_WLEN_7;
            break;
        default:
            bInitialized = FALSE;
    }

    if (bInitialized)
    {
        //Enable UART peripheral
        SysCtlPeripheralEnable(SYSCTL_PERIPH_UART0);

        //UART configuration
        UARTConfigSetExpClk(UART0_BASE, SysCtlClockGet(), ulBaudRate,
                            (ulBits | ulStopBits | ulParity));

        //Disable UART FIFO
        UARTFIFODisable(UART0_BASE);

        //Enable UART interrupts
        IntEnable(INT_UART0);
    }

    return bInitialized;
}

BOOL
xMBPortSerialPutByte(CHAR ucByte)
{
    /* Put a byte in the UARTs transmit buffer. This function is called
     * by the protocol stack if pxMBFrameCBTransmitterEmpty( ) has been
     * called. */
    BOOL bCharPutSuccess;
    bCharPutSuccess = UARTCharPutNonBlocking(UART0_BASE, (UCHAR)ucByte);

    return bCharPutSuccess;
}

BOOL
xMBPortSerialGetByte(CHAR * pucByte)
{
    /* Return the byte in the UARTs receive buffer. This function is called
     * by the protocol stack after pxMBFrameCBByteReceived( ) has been called.
     */

    if (UARTCharsAvail(UART0_BASE))
    {
        *pucByte = (CHAR) UARTCharGetNonBlocking(UART0_BASE);
        return TRUE;
    }
    else
    {
        return FALSE;
    }
}

void UARTIntHandler(void)
{
    ULONG ui32status;
    ui32status = UARTIntStatus(UART0_BASE, true);
    UARTIntClear(UART0_BASE, ui32status);

    /* Overrun error interrupt - increment diagnostic counter 8.
     */
    if((ui32status & UART_INT_OE) != 0u)
    {
        (void)xMBDiagUpdateDiagCounter(DIAG_COUNT_8);
        (void)xMBDiagSetCharOverrun(TRUE); //set overrun flag
    }

    /* Create an interrupt handler for the receive interrupt for your target
     * processor. This function should then call pxMBFrameCBByteReceived( ). The
     * protocol stack will then call xMBPortSerialGetByte( ) to retrieve the
     * character.
     */
    if((ui32status & UART_INT_RX) != 0u)
    {
        pxMBFrameCBByteReceived();
    }

    /* Create an interrupt handler for the transmit buffer empty interrupt
     * (or an equivalent) for your target processor. This function should then
     * call pxMBFrameCBTransmitterEmpty( ) which tells the protocol stack that
     * a new character can be sent. The protocol stack will then call
     * xMBPortSerialPutByte( ) to send the character.
     */
    if((ui32status & UART_INT_TX) != 0u)
    {
        pxMBFrameCBTransmitterEmpty();
    }
}

/*
 * mbregisters.c
 *
 *  Created on: 16 lip 2019
 *      Author: Ksawery Wieczorkowski-Rettinger
 */

#include "mb.h"
#include "mbutils.h"
#include "mbreg.h"
#include <limits.h>

/* ----------------------------------------- MODBUS Memory -----------------------------------------------*/

static USHORT usRegCoilsStart = REG_COILS_START;
static UCHAR  usRegCoilsBuf[(REG_COILS_NREGS + (CHAR_BIT - 1)) / CHAR_BIT];

static USHORT usRegDiscreteStart = REG_DISCRETE_START;
static UCHAR  usRegDiscreteBuf[(REG_DISCRETE_NREGS + (CHAR_BIT - 1)) / CHAR_BIT];

static USHORT usRegInputStart = REG_INPUT_START;
static USHORT usRegInputBuf[REG_INPUT_NREGS];

static USHORT usRegHoldingStart = REG_HOLDING_START;
static USHORT usRegHoldingBuf[REG_HOLDING_NREGS];

/*------------------------------------ MODBUS Callback Functions -----------------------------------------*/

/* Callback function - Read/Write Coils */
eMBErrorCode eMBRegCoilsCB(UCHAR * pucRegBuffer, USHORT usAddress, USHORT usNCoils, eMBRegisterMode eMode)
{
    eMBErrorCode eStatus = MB_ENOERR;
    USHORT usRegIndex;

    usAddress -= 1;

    if ((usAddress >= REG_COILS_START) && (usAddress + usNCoils <= REG_COILS_START + REG_COILS_NREGS))
    {
        usRegIndex = (USHORT) (usAddress - usRegCoilsStart);

        switch (eMode)
        {
            case MB_REG_READ:
            {
                while (usNCoils > 0)
                {
                    /* xMBUtilGetBits reads individual bits from a char array,
                     * starting from the LSB in each array element. */
                    UCHAR ucResult = xMBUtilGetBits(usRegCoilsBuf, usRegIndex, 1);

                    /* xMBUtilSetBits writes individual bits to a char array,
                     * starting from the LSB in each array element. */
                    xMBUtilSetBits(pucRegBuffer, usRegIndex - (usAddress - usRegCoilsStart), 1, ucResult);

                    usRegIndex++;
                    usNCoils--;
                }
                break;
            }

            case MB_REG_WRITE:
            {
                while (usNCoils > 0)
                {
                    UCHAR ucResult = xMBUtilGetBits(pucRegBuffer, usRegIndex - (usAddress - usRegCoilsStart), 1);

                    xMBUtilSetBits(usRegCoilsBuf, usRegIndex, 1, ucResult);

                    usRegIndex++;
                    usNCoils--;
                }
                break;
            }
        }
    }
    else
    {
        eStatus = MB_ENOREG;
    }

    return eStatus;
}

/* Callback function - Read Discrete Inputs */
eMBErrorCode eMBRegDiscreteCB(UCHAR * pucRegBuffer, USHORT usAddress, USHORT usNDiscrete)
{
    eMBErrorCode eStatus = MB_ENOERR;
    USHORT usRegIndex;

    usAddress -= 1;

    if ((usAddress >= REG_DISCRETE_START) && (usAddress + usNDiscrete <= REG_DISCRETE_START + REG_DISCRETE_NREGS))
    {
        usRegIndex = (USHORT) (usAddress - usRegDiscreteStart);

        while (usNDiscrete > 0)
        {
            UCHAR ucResult = xMBUtilGetBits(usRegDiscreteBuf, usRegIndex, 1);
            xMBUtilSetBits(pucRegBuffer, usRegIndex - (usAddress - usRegDiscreteStart), 1, ucResult);
            usRegIndex++;
            usNDiscrete--;
        }
    }
    else
    {
        eStatus = MB_ENOREG;
    }

    return eStatus;
}

/* Callback function - Read Input Register */
eMBErrorCode eMBRegInputCB(UCHAR * pucRegBuffer, USHORT usAddress, USHORT usNRegs)
{
    eMBErrorCode eStatus = MB_ENOERR;
    USHORT usRegIndex;

    usAddress -= 1;

    if ((usAddress >= REG_INPUT_START) && (usAddress + usNRegs <= REG_INPUT_START + REG_INPUT_NREGS))
    {
        usRegIndex = (USHORT) (usAddress - usRegInputStart);

        while (usNRegs > 0)
        {
            *pucRegBuffer++ = (UCHAR) (usRegInputBuf[usRegIndex] >> 8);
            *pucRegBuffer++ = (UCHAR) (usRegInputBuf[usRegIndex] & 0xFF);
            usRegIndex++;
            usNRegs--;
        }
    }
    else
    {
        eStatus = MB_ENOREG;
    }

    return eStatus;
}


/* Callback function - Read/Write Holding Register */
eMBErrorCode eMBRegHoldingCB(UCHAR * pucRegBuffer, USHORT usAddress, USHORT usNRegs, eMBRegisterMode eMode)
{
    eMBErrorCode eStatus = MB_ENOERR;
    USHORT usRegIndex;

    usAddress -= 1;

    if ((usAddress >= REG_HOLDING_START) && (usAddress + usNRegs <= REG_HOLDING_START + REG_HOLDING_NREGS))
    {
        usRegIndex = (USHORT) (usAddress - usRegHoldingStart);
        switch (eMode)
        {
            /* Pass current register values to the protocol stack. */
            case MB_REG_READ:
                while (usNRegs > 0)
                {
                    *pucRegBuffer++ = (UCHAR)(usRegHoldingBuf[usRegIndex] >> 8);
                    *pucRegBuffer++ = (UCHAR)(usRegHoldingBuf[usRegIndex] & 0xFF);
                    usRegIndex++;
                    usNRegs--;
                }
                break;

            /* Update current register values with new values from the
             * protocol stack. */
            case MB_REG_WRITE:
                while (usNRegs > 0)
                {
                    usRegHoldingBuf[usRegIndex] = *pucRegBuffer++ << 8;
                    usRegHoldingBuf[usRegIndex] |= *pucRegBuffer++;
                    usRegIndex++;
                    usNRegs--;
                }
        }
    }
    else
    {
        eStatus = MB_ENOREG;
    }

    return eStatus;
}

//Interal (application) Input Register Read/Write
BOOL xInternalRegInputReadWrite(USHORT * usInput, USHORT usAddress, USHORT usNRegs, BOOL bWrite)
{
    USHORT usRegIndex = 0;
    usAddress -= 1;

    if ((usAddress >= REG_INPUT_START) && (usAddress + usNRegs <= REG_INPUT_START + REG_INPUT_NREGS))
    {
        usRegIndex = (USHORT) (usAddress - usRegInputStart);

        if (bWrite)
        {
            while (usNRegs > 0)
            {
                usRegInputBuf[usRegIndex] = *usInput++;
                usRegIndex++;
                usNRegs--;
            }
        }
        else //read registers
        {
            while (usNRegs > 0)
            {
                *usInput++ = usRegInputBuf[usRegIndex];
                usRegIndex++;
                usNRegs--;
            }
        }
        return TRUE;
    }
    else
    {
        return FALSE;
    }
}


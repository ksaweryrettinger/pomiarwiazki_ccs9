/*
 * mbreg.h
 *
 *  Created on: 16 lip 2019
 *      Author: Ksawery Wieczorkowski-Rettinger
 */

#ifndef MBPORT_INCLUDE_MBREG_H_
#define MBPORT_INCLUDE_MBREG_H_

/* ----------------------- Defines ------------------------------------------*/

#define REG_COILS_START      (1)    //coils starting address
#define REG_COILS_NREGS      (3)    //number of coils

#define REG_DISCRETE_START   (1001) //discretes starting addres
#define REG_DISCRETE_NREGS   (0)    //number of discretes

#define REG_INPUT_START      (3001) //input registers starting address
#define REG_INPUT_NREGS      (5)    //number of input registers

#define REG_HOLDING_START    (4001) //holding registers starting address
#define REG_HOLDING_NREGS    (0)    //number of holding registers

                                    //Holding Register 1 -> Modbus configuratons
                                    //Holding Register 2 -> Application errors

/* ----------------------- Function declarations -----------------------------*/

BOOL xInternalRegInputReadWrite(USHORT * usInput, USHORT usAddress, USHORT usNRegs, BOOL bWrite);

//Other register functions are declared within the FreeMODBUS API.

#endif /* MBPORT_INCLUDE_MBREG_H_ */

/*
 * FreeModbus Diagnostics
 * Copyright (C) 2019 Ksawery Wieczorkowski-Rettinger <kwrettinger@gmail.com>
 *
 */

#ifndef MODBUS_INCLUDE_MBDIAG_H_
#define MODBUS_INCLUDE_MBDIAG_H_

/* ----------------------- Defines ------------------------------------------*/
#define NUM_DIAG_COUNT             (8)
#define NUM_EVENT_LOG_MAX          (64)
#define EVENT_LOG_BYTES_BASE       (6)

#define DIAG_PDU_MIN_LENGTH        (3)
#define DIAG_PDU_CMD_LENGTH        (5)
#define DIAG_OTHER_PDU_LENGTH      (1)

#define DIAG_COUNT_1               (1)
#define DIAG_COUNT_2               (2)
#define DIAG_COUNT_3               (3)
#define DIAG_COUNT_4               (4)
#define DIAG_COUNT_5               (5)
#define DIAG_COUNT_6               (6)
#define DIAG_COUNT_7               (7)
#define DIAG_COUNT_8               (8)

#define DIAG_STATUS_COMPLETE       (0x0000)
#define DIAG_STATUS_PROCESSING     (0xFFFF) //not possible in current implementation

#define DIAG_EVENT_RCV_NORMAL      (0b10000000)
#define DIAG_EVENT_RCV_COMM_ERR    (0b00000010)
#define DIAG_EVENT_RCV_OVERRUN     (0b00010000)
#define DIAG_EVENT_RCV_LISTEN      (0b00100000) //listen only mode not implemented
#define DIAG_EVENT_RCV_BROADCAST   (0b01000000)

#define DIAG_EVENT_SND_NORMAL      (0b01000000)
#define DIAG_EVENT_SND_EX_READ     (0b00000001)
#define DIAG_EVENT_SND_EX_ABORT    (0b00000010)
#define DIAG_EVENT_SND_EX_BUSY     (0b00000100)
#define DIAG_EVENT_SND_EX_NAK      (0b00001000)
#define DIAG_EVENT_SND_TIMEOUT     (0b00010000)
#define DIAG_EVENT_SND_LISTEN      (0b00100000) //listen only mode not implemented

#define DIAG_EVENT_COMMS_RESTART   (0b00000000)

/* ----------------------- Supporting functions -----------------------------*/

void xMBDiagUpdateDiagCounter(UCHAR countID);
void xMBDiagUpdateEventCounter(void);
void xMBDiagUpdateEventLog(CHAR ucEvent);
BOOL xMBDiagCheckData(UCHAR * pucFrameCur, UCHAR ucDataHigh, UCHAR ucDataLow);
BOOL xMBDiagIsCharOverrun(void);
void xMBDiagSetCharOverrun(BOOL bState);
BOOL xMBDiagIsCommsRestart(void);
void xMBDiagSetCommsRestart(BOOL bState);
BOOL xMBDiagIsClearCounters(void);
void xMBDiagSetClearCounters(BOOL bState);

#endif /* MODBUS_INCLUDE_MBDIAG_H_ */

/* 
 * FreeModbus Libary: A portable Modbus implementation for Modbus ASCII/RTU.
 * Copyright (c) 2006-2018 Christian Walter <cwalter@embedded-solutions.at>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. The name of the author may not be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 * NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 * THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

/* ----------------------- System includes ----------------------------------*/
#include "stdlib.h"
#include "string.h"
#include <limits.h>

/* ----------------------- Platform includes --------------------------------*/
#include "port.h"

/* ----------------------- Modbus includes ----------------------------------*/
#include "mb.h"
#include "mbframe.h"
#include "mbproto.h"
#include "mbconfig.h"
#include "mbdiag.h"
#include "mbutils.h"

/* ----------------------- Defines ------------------------------------------*/
#define MB_PDU_DIAG_SUBFUNC_OFF           (MB_PDU_DATA_OFF)
#define MB_PDU_DIAG_DATA_OFF              (MB_PDU_DATA_OFF + 2)
#define MB_PDU_DIAG_SUBFUNC_TO_COUNT      (0x000A)
#define MB_PDU_DIAG_NUM_EXCEPTIONS        (8)

/* ----------------------- Static variables ---------------------------------*/
static USHORT usDiagCount[NUM_DIAG_COUNT] = {0};
static UCHAR  ucEventLog[NUM_EVENT_LOG_MAX] = {0};
static UCHAR  ucEventLogLen = 0;
static USHORT usEventCount = 0;
static BOOL bCharOverrun = FALSE;
static BOOL bCommsRestart = FALSE;
static BOOL bClearCounters = FALSE;
static USHORT usStatus = DIAG_STATUS_COMPLETE;

/* ----------------------- Start implementation -----------------------------*/

#if MB_FUNC_DIAG_DIAGNOSTIC_ENABLED > 0
eMBException eMBFuncDiagDiagnostics(UCHAR * pucFrame, USHORT * usLen)
{
    UCHAR   usSubFuncCodeHigh;
    UCHAR   usSubFuncCodeLow;
    USHORT  usSubFuncCode;
    UCHAR   *pucFrameCur;
    UCHAR   i;

    eMBException eStatus = MB_EX_NONE;

    if (*usLen >= DIAG_PDU_MIN_LENGTH)
    {
        usSubFuncCodeHigh = pucFrame[MB_PDU_DIAG_SUBFUNC_OFF];
        usSubFuncCodeLow = pucFrame[MB_PDU_DIAG_SUBFUNC_OFF + 1];

        usSubFuncCode = (USHORT)(usSubFuncCodeHigh << 8);
        usSubFuncCode |= (USHORT)(usSubFuncCodeLow);

        if (usSubFuncCode != 0x0000) // if code = 0x0000, echo request data
        {
            if (*usLen == DIAG_PDU_CMD_LENGTH)
            {
                /* Set the current PDU data pointer to the beginning. */
                pucFrameCur = &pucFrame[MB_PDU_FUNC_OFF];
                *usLen = MB_PDU_FUNC_OFF;

                /* First bytes contain the function and sub-function codes. */
                *pucFrameCur++ = MB_FUNC_DIAG_DIAGNOSTIC;
                *pucFrameCur++ = usSubFuncCodeHigh;
                *pucFrameCur++ = usSubFuncCodeLow;
                *usLen += 3;

                if (usSubFuncCode == 0x0001) //restart communications
                {
                    if ((xMBDiagCheckData(pucFrame, 0x00, 0x00))
                            || (xMBDiagCheckData(pucFrame, 0xFF, 0x00)))
                    {
                        xMBDiagSetCommsRestart(TRUE);
                        memset(usDiagCount, 0, sizeof(usDiagCount)); //clear diagnostic counters
                        usEventCount = 0;  //clear event counter

                        if (xMBDiagCheckData(pucFrame, 0xFF, 0x00)) //also reset event log
                        {
                            ucEventLogLen = 0; //clear event log
                        }

                        *usLen += 2;
                    }
                    else
                    {
                        eStatus = MB_EX_ILLEGAL_DATA_VALUE;
                    }

                }
                else if (usSubFuncCode == 0x000A) //clear counters
                {
                    if (xMBDiagCheckData(pucFrame, 0x00, 0x00))
                    {
                        xMBDiagSetClearCounters(TRUE);
                        usEventCount = 0;  //clear event counter
                        memset(usDiagCount, 0, sizeof(usDiagCount)); //clear diagnostic counters
                        *usLen += 2;
                    }
                    else
                    {
                        eStatus = MB_EX_ILLEGAL_DATA_VALUE;
                    }
                }
                else if (usSubFuncCode == 0x0002) //return diagnostic register
                {
                    if (xMBDiagCheckData(pucFrame, 0x00, 0x00))
                    {
                        for (i = 1; i <= NUM_DIAG_COUNT; i++)
                        {
                            *pucFrameCur++ = (UCHAR) (usDiagCount[i - 1] >> 8);
                            *pucFrameCur++ = (UCHAR) (usDiagCount[i - 1] & 0xFF);
                            *usLen += 2;
                        }
                    }
                    else
                    {
                        eStatus = MB_EX_ILLEGAL_DATA_VALUE;
                    }
                }
                else if (usSubFuncCode == 0x0014) //clear overrun counter
                {
                    if (xMBDiagCheckData(pucFrame, 0x00, 0x00))
                    {
                        usDiagCount[DIAG_COUNT_8 - 1] = 0;
                        *usLen += 2;
                    }
                    else
                    {
                        eStatus = MB_EX_ILLEGAL_DATA_VALUE;
                    }
                }
                else if ((usSubFuncCode >= 0x000B) && (usSubFuncCode <= 0x0012)) //read individual counters
                {
                    if (xMBDiagCheckData(pucFrame, 0x00, 0x00))
                    {
                        *pucFrameCur++ = (UCHAR) (usDiagCount[usSubFuncCode - MB_PDU_DIAG_SUBFUNC_TO_COUNT - 1] >> 8);
                        *pucFrameCur++ = (UCHAR) (usDiagCount[usSubFuncCode - MB_PDU_DIAG_SUBFUNC_TO_COUNT - 1] & 0xFF);
                        *usLen += 2;
                    }
                    else
                    {
                        eStatus = MB_EX_ILLEGAL_DATA_VALUE;
                    }
                }
                else
                {
                    eStatus = MB_EX_ILLEGAL_FUNCTION;
                }
            }
            else
            {
                eStatus = MB_EX_ILLEGAL_DATA_VALUE;
            }
        }
    }
    else
    {
        eStatus = MB_EX_ILLEGAL_DATA_VALUE;
    }

    return eStatus;
}
#endif

#if MB_FUNC_DIAG_READ_EXCEPTION_ENABLED > 0
eMBException eMBFuncDiagReadException(UCHAR * pucFrame, USHORT * usLen)
{
    eMBException eStatus = MB_EX_NONE;

    UCHAR *pucFrameCur;
    UCHAR ucExceptionStatus = 0;

    if (*usLen == DIAG_OTHER_PDU_LENGTH)
    {
        /* Set the current PDU data pointer to the beginning. */
        pucFrameCur = &pucFrame[MB_PDU_FUNC_OFF];
        *usLen = MB_PDU_FUNC_OFF;

        /* First byte contains the function code. */
        *pucFrameCur++ = MB_FUNC_DIAG_READ_EXCEPTION;
        *usLen += 1;

        /* Determine which exceptions are defined */
        #if MB_EX_ILLEGAL_FUNCTION_ENABLED > 0
        xMBUtilSetBits(&ucExceptionStatus, 0, 1, 1);
        #endif

        #if MB_EX_ILLEGAL_DATA_ADDRESS_ENABLED > 0
        xMBUtilSetBits(&ucExceptionStatus, 1, 1, 1);
        #endif

        #if MB_EX_ILLEGAL_DATA_VALUE_ENABLED > 0
        xMBUtilSetBits(&ucExceptionStatus, 2, 1, 1);
        #endif

        #if MB_EX_SLAVE_DEVICE_FAILURE_ENABLED > 0
        xMBUtilSetBits(&ucExceptionStatus, 3, 1, 1);
        #endif

        #if MB_EX_ACKNOWLEDGE_ENABLED > 0
        xMBUtilSetBits(&ucExceptionStatus, 4, 1, 1);
        #endif

        #if MB_EX_SLAVE_BUSY_ENABLED > 0
        xMBUtilSetBits(&ucExceptionStatus, 5, 1, 1);
        #endif

        #if MB_EX_NEGATIVE_ACKNOWLEDGE_ENABLED > 0
        xMBUtilSetBits(&ucExceptionStatus, 6, 1, 1);
        #endif

        #if MB_EX_MEMORY_PARITY_ERROR_ENABLED > 0
        xMBUtilSetBits(&ucExceptionStatus, 7, 1, 1);
        #endif

        *pucFrameCur++ = ucExceptionStatus;
        *usLen += 1;
    }
    else
    {
        eStatus = MB_EX_ILLEGAL_DATA_VALUE;
    }

    return eStatus;
}
#endif

#if MB_FUNC_DIAG_GET_COM_EVENT_CNT_ENABLED > 0
eMBException eMBFuncDiagGetCommEventCnt(UCHAR * pucFrame, USHORT * usLen)
{
    eMBException eStatus = MB_EX_NONE;
    UCHAR *pucFrameCur;

    if (*usLen == DIAG_OTHER_PDU_LENGTH)
    {
        /* Set the current PDU data pointer to the beginning. */
        pucFrameCur = &pucFrame[MB_PDU_FUNC_OFF];
        *usLen = MB_PDU_FUNC_OFF;

        /* First byte contains the function code. */
        *pucFrameCur++ = MB_FUNC_DIAG_GET_COM_EVENT_CNT;
        *usLen += 1;

        /* Two-byte status word */
        *pucFrameCur++ = (UCHAR) (usStatus >> 8);
        *pucFrameCur++ = (UCHAR) (usStatus & 0xFF);
        *usLen += 2;

        /* Two-byte event counter */
        *pucFrameCur++ = (UCHAR) (usEventCount >> 8);
        *pucFrameCur++ = (UCHAR) (usEventCount & 0xFF);
        *usLen += 2;
    }
    else
    {
        eStatus = MB_EX_ILLEGAL_DATA_VALUE;
    }

    return eStatus;
}
#endif

#if MB_FUNC_DIAG_GET_COM_EVENT_LOG_ENABLED > 0
eMBException eMBFuncDiagGetCommEventLog(UCHAR * pucFrame, USHORT * usLen)
{
    eMBException eStatus = MB_EX_NONE;
    UCHAR *pucFrameCur;
    UCHAR i;

    if (*usLen == DIAG_OTHER_PDU_LENGTH)
    {
        /* Set the current PDU data pointer to the beginning. */
        pucFrameCur = &pucFrame[MB_PDU_FUNC_OFF];
        *usLen = MB_PDU_FUNC_OFF;

        /* First byte contains the function code. */
        *pucFrameCur++ = MB_FUNC_DIAG_GET_COM_EVENT_LOG;
        *usLen += 1;

        /* Byte Count */
        *pucFrameCur++ = ucEventLogLen + EVENT_LOG_BYTES_BASE;
        *usLen += 1;

        /* Two-byte status word */
        *pucFrameCur++ = (UCHAR) (usStatus >> 8);
        *pucFrameCur++ = (UCHAR) (usStatus & 0xFF);
        *usLen += 2;

        /* Two-byte event counter */
        *pucFrameCur++ = (UCHAR) (usEventCount >> 8);
        *pucFrameCur++ = (UCHAR) (usEventCount & 0xFF);
        *usLen += 2;

        /* Two-byte message counter */
        *pucFrameCur++ = (UCHAR) (usDiagCount[DIAG_COUNT_1 - 1] >> 8);
        *pucFrameCur++ = (UCHAR) (usDiagCount[DIAG_COUNT_1 - 1] & 0xFF);
        *usLen += 2;

        for (i = 0; i < ucEventLogLen; i++)
        {
            *pucFrameCur++ = ucEventLog[i];
            *usLen += 1;
        }
    }
    else
    {
        eStatus = MB_EX_ILLEGAL_DATA_VALUE;
    }

    return eStatus;
}
#endif

/* Increment single counter value */
void xMBDiagUpdateDiagCounter(UCHAR ucCountNum)
{
    if ((ucCountNum > 0) && (ucCountNum <= NUM_DIAG_COUNT))
    {
        if (usDiagCount[ucCountNum - 1] < USHRT_MAX)
        {
            usDiagCount[ucCountNum - 1]++;
        }
    }
}

/* Update event counter */
void xMBDiagUpdateEventCounter(void)
{
    if (usEventCount < USHRT_MAX)
    {
        usEventCount++;
    }
}

/* Update event log */
void xMBDiagUpdateEventLog(CHAR ucEvent)
{
    UCHAR i;

    if (ucEventLogLen < NUM_EVENT_LOG_MAX)
    {
        ucEventLogLen++;
    }

    /* Shift buffer elements (remove oldest event if full) */
    for (i = ucEventLogLen - 1; i > 0; i--)
    {
        ucEventLog[i] = ucEventLog[i-1];
    }

    ucEventLog[0] = ucEvent;
}

/* Check if incoming diagnostic data fields are correct */
BOOL xMBDiagCheckData(UCHAR * pucFrameCur, UCHAR ucDataHigh, UCHAR ucDataLow)
{
    if ((pucFrameCur[MB_PDU_DIAG_DATA_OFF] == ucDataHigh)
            && (pucFrameCur[MB_PDU_DIAG_DATA_OFF + 1] == ucDataLow))
    {
        return TRUE;
    }
    return FALSE;
}

/* Read status of character overrun flag */
BOOL xMBDiagIsCharOverrun(void)
{
    return bCharOverrun;
}

/* Set character overrun flag */
void xMBDiagSetCharOverrun(BOOL bState)
{
    bCharOverrun = bState;
}

/* Determine if communications restart event just occurred */
BOOL xMBDiagIsCommsRestart(void)
{
    return bCommsRestart;
}

/* Set communications restart event flag */
void xMBDiagSetCommsRestart(BOOL bState)
{
    bCommsRestart = bState;
}

/* Determine if communications restart event just occurred */
BOOL xMBDiagIsClearCounters(void)
{
    return bClearCounters;
}

/* Set communications restart event flag */
void xMBDiagSetClearCounters(BOOL bState)
{
    bClearCounters = bState;
}


/* 
 * FreeModbus Libary: A portable Modbus implementation for Modbus ASCII/RTU.
 * Copyright (c) 2006-2018 Christian Walter <cwalter@embedded-solutions.at>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. The name of the author may not be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 * NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 * THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

/* ----------------------- System includes ----------------------------------*/
#include "stdlib.h"
#include "string.h"
#include <limits.h>

/* ----------------------- Platform includes --------------------------------*/
#include "port.h"

/* ----------------------- Modbus includes ----------------------------------*/
#include "mb.h"
#include "mbframe.h"
#include "mbproto.h"
#include "mbconfig.h"
#include "mbdiag.h"

/* ----------------------- Defines ------------------------------------------*/
#define MB_MIN_SLAVEID_LENGTH       (2)

#if MB_FUNC_OTHER_REP_SLAVEID_ENABLED > 0

/* ----------------------- Static variables ---------------------------------*/
static UCHAR   ucMBSlaveID[MB_FUNC_OTHER_REP_SLAVEID_BUF];
static UCHAR   ucMBSlaveIDLen;

/* ----------------------- Start implementation -----------------------------*/

eMBErrorCode
eMBSetSlaveID( UCHAR ucSlaveID, BOOL xIsRunning,
               UCHAR const *pucAdditional, UCHAR usAdditionalLen )
{
    eMBErrorCode    eStatus = MB_ENOERR;

    /* the first byte and second byte in the buffer is reserved for
     * the parameter ucSlaveID and the running flag. The rest of
     * the buffer is available for additional data. */
    if (usAdditionalLen + MB_MIN_SLAVEID_LENGTH < MB_FUNC_OTHER_REP_SLAVEID_BUF)
    {
        ucMBSlaveIDLen = 0;
        ucMBSlaveID[ucMBSlaveIDLen++] = ucSlaveID;
        ucMBSlaveID[ucMBSlaveIDLen++] = (UCHAR) (xIsRunning ? 0xFF : 0x00);
        if (usAdditionalLen > 0)
        {
            memcpy(&ucMBSlaveID[ucMBSlaveIDLen], pucAdditional, (size_t) usAdditionalLen);
            ucMBSlaveIDLen += usAdditionalLen;
        }
    }
    else
    {
        eStatus = MB_ENORES;
    }
    return eStatus;
}

eMBException
eMBFuncReportSlaveID( UCHAR * pucFrame, USHORT * usLen )
{
    UCHAR   *pucFrameCur;
    UCHAR   i;

    if (*usLen == DIAG_OTHER_PDU_LENGTH)
    {
        if (ucMBSlaveIDLen >= MB_MIN_SLAVEID_LENGTH)
        {
            /* Set the current PDU data pointer to the beginning. */
            pucFrameCur = &pucFrame[MB_PDU_FUNC_OFF];
            *usLen = MB_PDU_FUNC_OFF;

            /* First byte contains the function code. */
            *pucFrameCur++ = MB_FUNC_OTHER_REPORT_SLAVEID;
            *usLen += 1;

            /* Number of bytes */
            *pucFrameCur++ = ucMBSlaveIDLen;
            *usLen += 1;

            for (i = 0; i < ucMBSlaveIDLen; i++)
            {
                *pucFrameCur++ = ucMBSlaveID[i];
                *usLen += 1;
            }

            return MB_EX_NONE;
        }
        else
        {
            return MB_EX_SLAVE_DEVICE_FAILURE;
        }
    }
    else
    {
        return MB_EX_ILLEGAL_DATA_VALUE;
    }
}

#endif
